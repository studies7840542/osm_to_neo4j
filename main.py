from neo4j import GraphDatabase  # Import the Neo4j Python driver
import neo4j
import osmnx as ox  # Import the osmnx library for working with OpenStreetMap data
import time

# Fetch road network data for Kraków, Poland using osmnx and visualize it
G = ox.graph_from_place("Kraków, Poland", network_type="drive")
fig, ax = ox.plot_graph(G, show=False)

# Convert the road network data into GeoDataFrames for nodes and relationships
gdf_nodes, gdf_relationships = ox.graph_to_gdfs(G)

# Reset the index of the GeoDataFrames for consistency
gdf_nodes.reset_index(inplace=True)
gdf_relationships.reset_index(inplace=True)

# Define a class for managing the connection to the Neo4j database
class Neo4jConnection:
    def __init__(self, uri, user, pwd):
        self.__uri = uri  # Neo4j database URI
        self.__user = user  # Username for database authentication
        self.__pwd = pwd  # Password for database authentication
        self.__driver = None  # Initialize the Neo4j driver as None
        try:
            # Create a Neo4j driver for connecting to the database
            self.__driver = GraphDatabase.driver(self.__uri, auth=(self.__user, self.__pwd))
        except Exception as e:
            print("Failed to create the driver:", e)

    def close(self):
        if self.__driver is not None:
            self.__driver.close()

    def query(self, query, parameters=None, db=None):
        assert self.__driver is not None, "Driver not initialized!"  # Check if the driver is initialized
        session = None
        response = None
        try:
            # Open a session and run a Neo4j Cypher query
            session = self.__driver.session(database=db) if db is not None else self.__driver.session()
            response = list(session.run(query, parameters))
        except Exception as e:
            print("Query failed:", e)
        finally:
            if session is not None:
                session.close()
        return response

# Create a Neo4jConnection instance to connect to the Neo4j database
conn = Neo4jConnection(uri="bolt+s://d8427d00.databases.neo4j.io", user="neo4j", pwd="13uzlIcAiZRDEhsHOrFYxq1LF60UczueG-mmFi8J0x8")

# Function to batch insert data into the Neo4j database
def insert_data(query, rows, batch_size=10000):
    total = 0
    batch = 0
    start = time.time()
    result = None

    # Loop through the rows and insert data in batches
    while batch * batch_size < len(rows):
        res = conn.query(query, parameters = {'rows': rows[batch*batch_size:(batch+1)*batch_size].to_dict('records')})
        total += res[0]['total']
        batch += 1
        result = {"total": total, "batches": batch, "time": time.time() - start}
        print(result)
    return result

# Function to load the road network data into the Neo4j database
def load_graph(nodes, rels, batch_size=10000):
    node_query = '''
    UNWIND $rows AS row
    WITH row WHERE row.osmid IS NOT NULL
    MERGE (i:Intersection {osmid: row.osmid})
        SET i.location = point({latitude: row.y, longitude: row.x }),
            i.ref = row.ref,
            i.highway = row.highway,
            i.street_count = toInteger(row.street_count)
    RETURN COUNT(*) as total
    '''

    # Insert node data into the Neo4j database
    nodes_result = insert_data(node_query, nodes, batch_size)

    rels_query = '''
    UNWIND $rows AS road
    MATCH (u:Intersection {osmid: road.u})
    MATCH (v:Intersection {osmid: road.v})
    MERGE (u)-[r:ROAD_SEGMENT {osmid: road.osmid}]->(v)
        SET r.oneway = road.oneway,
            r.lanes = road.lanes,
            r.ref = road.ref,
            r.name = road.name,
            r.highway = road.highway,
            r.max_speed = road.maxspeed,
            r.length = toFloat(road.length)
    RETURN COUNT(*) AS total
    '''

    # Insert relationship data into the Neo4j database
    rels_result = insert_data(rels_query, rels, batch_size)

    # Return the results of node and relationship insertions
    return {'node_results': 'nodes_result', 'rels_result': rels_result}

# Load the road network data into the Neo4j database
load_graph(gdf_nodes.drop(columns=['geometry']), gdf_relationships.drop(columns=['geometry']))
